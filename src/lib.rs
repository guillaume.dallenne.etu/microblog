pub use sqlx;
use sqlx::postgres::{PgPoolOptions, PgQueryResult, PgRow};
use sqlx::types::chrono::NaiveDateTime;
use sqlx::types::Uuid;
use sqlx::{PgPool, Row};

#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Message {
    pub id: Uuid,
    pub content: String,
    pub publication_date: NaiveDateTime,
    pub replies_to: Option<Uuid>,
    pub username: String,
    pub user_id: Uuid,
}

#[derive(Debug, Clone)]
pub struct Microblog {
    pool: PgPool,
}

impl Microblog {
    pub async fn connect(host: &str, max_connections: u32) -> sqlx::Result<Self> {
        let pool = PgPoolOptions::new()
            .max_connections(max_connections)
            .connect(&format!("postgresql://common_user@{}/microblog", host))
            .await?;

        Ok(Self { pool })
    }

    /// Publish a post for `user`. If `replies_to` is some message id,
    /// will act as a reply to. Returns the uuid of the post.
    pub async fn publish_post(
        &self,
        user: impl Into<(&str, &str)>,
        content: &str,
        replies_to: Option<Uuid>,
    ) -> sqlx::Result<Uuid> {
        let (username, password) = user.into();
        sqlx::query("SELECT insert_message($1, $2, $3, $4)")
            .bind(username)
            .bind(password)
            .bind(content)
            .bind(replies_to)
            .map(|row: PgRow| row.get(0))
            .fetch_one(&self.pool)
            .await
    }

    /// Make `user` follow `to_follow`.
    pub async fn follow(
        &self,
        user: impl Into<(&str, &str)>,
        to_follow: Uuid,
    ) -> sqlx::Result<PgQueryResult> {
        let (username, password) = user.into();
        sqlx::query("SELECT follow($1, $2, $3)")
            .bind(username)
            .bind(password)
            .bind(to_follow)
            .execute(&self.pool)
            .await
    }

    /// Get the `limit` (defaults to 50) first messages of the feed of `user`.
    pub async fn get_feed(
        &self,
        user: impl Into<(&str, &str)>,
        limit: Option<u32>,
    ) -> sqlx::Result<Vec<Message>> {
        let (username, password) = user.into();
        sqlx::query("SELECT * FROM feed($1, $2) LIMIT $3")
            .bind(username)
            .bind(password)
            .bind(limit.unwrap_or(50))
            .map(|row: PgRow| Message {
                id: row.get(0),
                content: row.get(1),
                publication_date: row.get(2),
                replies_to: row.get(3),
                username: row.get(4),
                user_id: Uuid::default(),
            })
            .fetch_all(&self.pool)
            .await
    }

    /// Get last posted messages.
    pub async fn get_messages(&self) -> sqlx::Result<Vec<Message>> {
        sqlx::query("SELECT * FROM messages")
            .map(|row: PgRow| Message {
                id: row.get(0),
                content: row.get(1),
                publication_date: row.get(2),
                replies_to: row.get(3),
                username: row.get(4),
                user_id: row.get(5),
            })
            .fetch_all(&self.pool)
            .await
    }
}
